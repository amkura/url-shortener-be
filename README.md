## Description

This is an Url Shortening test project done for the purpose of testing the skills learned during courses.

## Hashing Alghorithm

Hashing is done by nanoid package, which, in turn, uses crypto and an alphabet as its' main source for generating random URL-friendly ids.

## Database

The database used is MongoDB together with Mongoose helper package to easily query required data. Each generated URL belongs to a single User, and a User can have many URLs.

## Design Patterns

NestJS framework utilizes MVC design. The incoming requests get handled by a controller, where a Service is used to handle the primary logic and return the result.
