import {
  Body,
  Controller,
  Post,
  ValidationPipe,
  UseGuards,
  Request,
} from '@nestjs/common';
import { UserService } from './user.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { LocalAuthGuard } from './guards/local-auth.guard';

@Controller('v1/user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('/login')
  @UseGuards(LocalAuthGuard)
  login(@Request() req) {
    console.log('Req: ', req);
    return this.userService.login(req.user);
  }

  @Post('/signup')
  signup(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto,
  ): Promise<void> {
    return this.userService.signup(authCredentialsDto);
  }
}
