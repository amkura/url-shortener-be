import { Controller, Get, Param, Res } from '@nestjs/common';
import { UrlService } from './url/url.service';

@Controller()
export class AppController {
  constructor(private readonly urlService: UrlService) {}

  @Get(':urlCode')
  async getShortenUrl(@Param('urlCode') urlCode: string, @Res() res) {
    const url = await this.urlService.getShortenUrl(urlCode);
    res.redirect(url.longUrl);
  }
}
