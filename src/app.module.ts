import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './user/auth.module';
import { ConfigModule } from '@nestjs/config';
import { UrlModule } from './url/url.module';
import { UrlService } from './url/url.service';
import { UrlController } from './url/url.controller';
import { UrlSchema } from './url/schemas/url.schema';

const ENV = process.env.NODE_ENV;
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: !ENV ? '.env.prod' : `.env.${ENV}`,
      isGlobal: true,
    }),
    MongooseModule.forFeature([{ name: 'Url', schema: UrlSchema }]),
    MongooseModule.forRoot(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }),
    AuthModule,
    UrlModule,
  ],
  controllers: [AppController, UrlController],
  providers: [AppService, UrlService],
})
export class AppModule {}
