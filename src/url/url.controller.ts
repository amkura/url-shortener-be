import {
  Body,
  Controller,
  Post,
  ValidationPipe,
  UseGuards,
  Inject,
  Get,
  Delete,
  Param,
  Query,
} from '@nestjs/common';
import { UrlToShortenDto } from './dto/url-to-shorten.dto';
import { JwtAuthGuard } from '../user/guards/jwt-auth.guard';
import { UrlService } from './url.service';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { Url } from './interfaces/url.interface';

@Controller('v1/urls')
@UseGuards(JwtAuthGuard)
export class UrlController {
  constructor(
    private urlService: UrlService,
    @Inject(REQUEST) private readonly request: Request,
  ) {}

  @Get()
  getUserUrls(
    @Query() query,
  ): Promise<{ items: Url[]; count: number; total: number }> {
    return this.urlService.getUserUrls(
      this.request.user,
      +query.offset,
      +query.limit,
    );
  }

  @Post()
  shortenUrl(
    @Body(ValidationPipe) urlToShortenDto: UrlToShortenDto,
  ): Promise<Url> {
    return this.urlService.shortenUrl(urlToShortenDto, this.request.user);
  }

  @Delete('/:id')
  deleteUrl(@Param('id') id) {
    return this.urlService.deleteUrl(id);
  }
}
