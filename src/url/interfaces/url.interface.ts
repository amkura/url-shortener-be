import { Document } from 'mongoose';

export interface Url extends Document {
  readonly urlCode: string;
  readonly longUrl: string;
  readonly shortUrl: string;
  title: string;
}
