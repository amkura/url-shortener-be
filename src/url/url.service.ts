import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Url } from './interfaces/url.interface';
import { UrlToShortenDto } from './dto/url-to-shorten.dto';
import { nanoid } from 'nanoid';

@Injectable()
export class UrlService {
  constructor(@InjectModel('Url') private readonly urlModel: Model<Url>) {}

  async shortenUrl(
    urlToShortenDto: UrlToShortenDto,
    user: Express.User,
  ): Promise<Url> {
    const { url, title } = urlToShortenDto;

    let foundUrl = await this.urlModel.findOne({ longUrl: url, user });

    if (!foundUrl) {
      const urlCode = nanoid(8);
      const shortUrl = process.env.BASE_URL + urlCode;
      foundUrl = new this.urlModel({
        longUrl: url,
        shortUrl,
        urlCode,
        title,
        user,
      });

      await foundUrl.save();
    }

    if (foundUrl.title !== title) {
      foundUrl.title = title;
      await foundUrl.save();
    }

    return foundUrl;
  }

  async getUserUrls(
    user: Express.User,
    offset = 0,
    limit = 5,
  ): Promise<{
    items: Url[];
    count: number;
    total: number;
  }> {
    const [urls, total] = await Promise.all([
      this.urlModel.find({ user }).skip(offset).limit(limit),
      this.urlModel.countDocuments(),
    ]);

    return {
      items: urls,
      count: urls.length,
      total,
    };
  }

  async deleteUrl(id: string) {
    return await this.urlModel.deleteOne({ _id: id });
  }

  async getShortenUrl(urlCode: string): Promise<Url> {
    return this.urlModel.findOne({ urlCode });
  }
}
