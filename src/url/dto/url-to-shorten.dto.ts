import { IsString, IsUrl } from 'class-validator';

export class UrlToShortenDto {
  @IsString()
  title: string;

  @IsString()
  @IsUrl()
  url: string;
}
